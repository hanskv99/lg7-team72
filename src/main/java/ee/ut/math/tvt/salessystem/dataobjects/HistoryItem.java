package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Entity
@Table(name= "HISTORYITEM")
public class HistoryItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "DATE")
    private LocalDate date;
    @Column(name = "TIME")
    private LocalTime time;
    @Column(name = "TOTAL")
    double total;
    @Transient
    private List<SoldItem> soldItemList;

    public HistoryItem(double total) {
        this.date = LocalDate.now();
        this.time = LocalTime.now();
        this.total = total;
    }

    public HistoryItem() {

    }

    public List<SoldItem> getSoldItemList() {
        return soldItemList;
    }

    public void setSoldItemList(List<SoldItem> soldItemList) {
        this.soldItemList = soldItemList;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "HistoryItem{" +
                "soldItemList=" + soldItemList +
                ", date=" + date +
                ", time=" + time +
                ", total=" + total +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
