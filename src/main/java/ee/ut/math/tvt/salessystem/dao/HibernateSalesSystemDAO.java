package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;


    public HibernateSalesSystemDAO() {
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    public void close() {
        em.close();
        emf.close();
    }


    @Override
    public EntityManager getEm() {
        return em;
    }

    @Override
    public List<StockItem> getStockItemList() {
        return null;
    }

    @Override
    public void changeQuantity(long id, int sold) {
        StockItem detached = em.find(StockItem.class, id);
        int newQuantity = detached.getQuantity() - sold;
        if (newQuantity < 0) throw new IllegalArgumentException("Item amount exceedes available stock of:" + detached.getQuantity());
        if (newQuantity == 0) {
            em.remove(detached);
        }
        else {
            detached.setQuantity(detached.getQuantity() - sold);
            StockItem managed = em.merge(detached);
        }
    }

    @Override
    public List<StockItem> findStockItems() {
        return em.createQuery("from StockItem", StockItem.class).getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        return em.find(StockItem.class, id);
    }

    @Override
    public void updateStockItemQuantity(long id, int quantity) {
        if (quantity < 1) throw new IllegalArgumentException("Can not add stockitem with a quantity of < 1");
        StockItem detached = em.find(StockItem.class, id);
        detached.setQuantity(quantity);
        StockItem managed = em.merge(detached);
    }

    @Override
    public void updateStockItemPrice(long id, double price) {
        if (price <= 0) throw new IllegalArgumentException("Can not add stockitem with a price of <= 0");
        StockItem detached = em.find(StockItem.class, id);
        detached.setPrice(price);
        StockItem managed = em.merge(detached);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        if (stockItem.getQuantity() < 1) throw new IllegalArgumentException("Can not add stockitem with a quantity of < 1");
        if (stockItem.getPrice() <= 0) throw new IllegalArgumentException("Can not add stockitem with a price of <= 0");
        beginTransaction();
        em.persist(stockItem);
        commitTransaction();
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        em.persist(item);
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

    @Override
    public void clearTransactions() {
        em.clear();
    }

    @Override
    public void persistHistoryItem(HistoryItem historyItem) {
        em.persist(historyItem);
    }

    @Override
    public List<HistoryItem> getHistoryItemList() {
        return em.createQuery("from HistoryItem", HistoryItem.class).getResultList();
    }
    @Override
    public List<SoldItem> getSoldItemList(Long id) {
        return em.createQuery("from SoldItem where History_id = " + id, SoldItem.class).getResultList();
    }
    @Override
    public List<HistoryItem> showLast10History() {
        return em.createQuery("from HistoryItem order by Date desc, Time desc", HistoryItem.class).setMaxResults(10).getResultList();
    }

    @Override
    public List<HistoryItem> showDatesBetween(LocalDate date1, LocalDate date2) {
        return em.createQuery("from HistoryItem where Date between '" + date1 + "' and '" + date2 +"'", HistoryItem.class).getResultList();
    }
}