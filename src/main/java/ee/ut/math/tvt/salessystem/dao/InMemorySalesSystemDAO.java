package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<HistoryItem> historyItemList;
    private int transactionBegin;
    private int transactionCommit;


    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));

        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.historyItemList = new ArrayList<>();
    }

    public List<StockItem> getStockItemList() {
        return stockItemList;
    }

    @Override
    public void changeQuantity(long id, int sold) {
        StockItem stockItem = findStockItem(id);
        if(stockItem.getQuantity() - sold < 0) throw new IllegalArgumentException("Item amount exceedes available stock of:" + stockItem.getQuantity());
        stockItem.setQuantity(stockItem.getQuantity()-sold);
    }

    public int getTransactionBegin() {
        return transactionBegin;
    }

    public int getTransactionCommit() {
        return transactionCommit;
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public EntityManager getEm() {
        return null;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void updateStockItemQuantity(long id, int quantity) {
        if (quantity < 1) throw new IllegalArgumentException("Can not add stockitem with a quantity of < 1");
        StockItem stockItem = null;
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                stockItem = item;
        }
        stockItem.setQuantity(stockItem.getQuantity() + quantity);
    }

    @Override
    public void updateStockItemPrice(long id, double price) {
        if (price <= 0) throw new IllegalArgumentException("Can not add stockitem with a price of <= 0");
        StockItem stockItem = null;
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                stockItem = item;
        }
        stockItem.setPrice(price);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        if (stockItem.getQuantity() < 1) throw new IllegalArgumentException("Can not add stockitem with a quantity of < 1");
        if (stockItem.getPrice() <= 0) throw new IllegalArgumentException("Can not add stockitem with a price of <= 0");
        beginTransaction();
        stockItemList.add(stockItem);
        commitTransaction();
    }

    @Override
    public void beginTransaction() {
        transactionBegin = 1;
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
        transactionCommit = 1;
    }

    @Override
    public void clearTransactions() {

    }

    @Override
    public void persistHistoryItem(HistoryItem historyItem) {
        historyItemList.add(historyItem);

    }


    public List<HistoryItem> getHistoryItemList() {
        return historyItemList;
    }

    @Override
    public List<SoldItem> getSoldItemList(Long id) {
        ArrayList<SoldItem> oneItemList = new ArrayList();
        for (SoldItem soldItem : soldItemList) {
            if (soldItem.getBarcode().equals(id)){
                oneItemList.add(soldItem);
            }
        }
        return oneItemList;
    }

    @Override
    public List<HistoryItem> showLast10History() {
        return null;
    }

    @Override
    public List<HistoryItem> showDatesBetween(LocalDate date1, LocalDate date2) {
        return null;
    }
}
