package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {

        int stockItemQuantity = dao.findStockItem(item.getBarcode()).getQuantity();
        if (item.getQuantity() > stockItemQuantity) {
            throw new SalesSystemException("Quantity exceeds warehouse stock of the item");
        }
        boolean exists = false;
        for (SoldItem soldItem : items) {
            if (soldItem.getBarcode() == item.getBarcode()) {
                exists = true;
                if((soldItem.getQuantity() + item.getQuantity()) < 0) {
                    throw new SalesSystemException("Can't remove more than is in the cart");
                }
                if ((soldItem.getQuantity() + item.getQuantity()) <= stockItemQuantity) {
                    soldItem.setQuantity(soldItem.getQuantity() + item.getQuantity());
                }
                else {
                    log.info("Item quantity exceeds available stock of: " + stockItemQuantity);
                    throw new SalesSystemException("Quantity exceeds warehouse stock of the item");
                }
                if(soldItem.getQuantity() == 0) {
                    items.remove(soldItem);
                }
                break;
            }
        }
        if (!exists) {
            if (item.getQuantity() < 0) {
                throw new SalesSystemException("Quantity can only be negative if item is in cart for removing");
            }
        }
        if (!exists) items.add(item);
        log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());

    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {
        HistoryItem newHistoryItem = new HistoryItem(sumOfItems());
        dao.beginTransaction();
        dao.persistHistoryItem(newHistoryItem);
        Long historyItemId = newHistoryItem.getId();
        log.info("New transaction started");
        try {
            for (SoldItem item : items) {
                dao.changeQuantity(item.getBarcode(), item.getQuantity());
                log.debug(item.getBarcode() + " - id: item removed from warehouse");
                item.setHistory_id(historyItemId);
                dao.saveSoldItem(item);
                log.debug("Sold item saved to memory");
            }
            dao.commitTransaction();
            items.clear();
            log.debug("All items sold from cart");

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            dao.rollbackTransaction();
        }
    }

    public double sumOfItems(){
        double sum = 0.0;
        for (SoldItem item : items) {
            sum += item.getPrice()*(double)(item.getQuantity());
        }
        return sum;
    }



}
