package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);
    private final SalesSystemDAO dao;
    private final ShoppingCart cart;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new HibernateSalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        log.info("Sales system started");
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
            log.debug("Command processed");
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
            log.debug("No items in stock");
        }
        System.out.println("-------------------------");
        log.debug("Stock items displayed");
    }

    private void showCart() {
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
        log.debug("Shopping cart shown");
    }
    private void removeItemFromStock(long index, int sold){
        dao.changeQuantity(index,sold);
        log.info("Item removed from stock, barcode: " + index);
    }

    private void addItemToStock() throws IOException {
        log.info("New stockitem adding started");
        boolean complete = false;
        while (!complete){
            try{
                System.out.println("Enter the items index (Integer)");
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                long index = Long.parseLong(in.readLine().trim().toLowerCase());
                if (dao.findStockItem(index)==null)  {
                    System.out.println("Adding new item");
                    System.out.println("Enter the items name (String)");
                    String name = in.readLine().trim().toLowerCase();
                    System.out.println("Enter the items description (String)");
                    String itemDescription = in.readLine().trim().toLowerCase();
                    System.out.println("Enter the items price (Integer)");
                    long price = Long.parseLong(in.readLine().trim().toLowerCase());
                    System.out.println("Enter the items quantity(Integer)");
                    int quantity = Integer.parseInt(in.readLine().trim().toLowerCase());
                    dao.saveStockItem(new StockItem(index,name,itemDescription,price,quantity));
                    log.info("New stock item added to warehouse");
                }
                else{
                    System.out.println("Adding existing item: " + dao.findStockItem(index).getName());
                    System.out.println("Enter the items price (Integer)");
                    long price = Long.parseLong(in.readLine().trim().toLowerCase());
                    System.out.println("Enter the items quantity(Integer)");
                    int quantity = Integer.parseInt(in.readLine().trim().toLowerCase());
                    dao.beginTransaction();
                    if (price != dao.findStockItem(index).getPrice()) dao.updateStockItemPrice(index, price);
                    dao.updateStockItemQuantity(index,quantity+dao.findStockItem(index).getQuantity());
                    dao.commitTransaction();
                    log.info("Stock item quantity updated in warehouse");
                }
                complete = true;
            }
            catch(Exception e) {
                dao.rollbackTransaction();
                log.debug("Input was incorrect");
                System.out.println("Incorrect input. Error message: " + e.getLocalizedMessage());
            }
        }
    }

    private void printUsage() {
        log.debug("Usage info displayed");
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("t\t\tShow team info");
        System.out.println("c\t\tShow cart contents");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("s\t\tAdd new item to warehouse stock");
        System.out.println("i IDX\tShow purchase details from history with index IDX");
        System.out.println("z\t\tShow purchase history");
        System.out.println("x\t\tShow last 10 purchases");
        System.out.println("b dd/mm/yyyy dd/mm/yyyy \t Show purchases between two dates");
        System.out.println("e IDX NR \tRemove NR of stock item with index IDX from the warehouse");
        System.out.println("-------------------------");
    }


    private void showTeamTab() {
        log.debug("Team info displayed");
        System.out.println("-------------------------");
        System.out.println("Team info: ");
        System.out.println("Team name\t\t\tLG7-Team72");
        System.out.println("Team leader\t\t\ttext");
        System.out.println("Team leader email\ttest@email.com");
        System.out.println("Team members\t\tAlan Kolk, Hans Kristjan Veri, Raiki Mänd");
        System.out.println("-------------------------");
    }

    private void showAllHistory() throws IOException {
        System.out.println("Purchase history");
        System.out.println("-------------------------");
        List<HistoryItem> historyItems = dao.getHistoryItemList();
        for (HistoryItem item : historyItems) {
            System.out.println(item.getId() + " Date: " + item.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + ", time: " + item.getTime() + ", total: " + item.getTotal());
        }
        System.out.println("-------------------------");
        log.debug("All purchases displayed");
    }

    private void showHistoryOfLast10() {
        System.out.println("History of last 10 purchases");
        System.out.println("-------------------------");
        for(HistoryItem item : dao.showLast10History()) {
            System.out.println(item.getId() + " Date: " + item.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + ", time: " + item.getTime() + ", total: " + item.getTotal());
        }
        System.out.println("-------------------------");
        log.debug("Last 10 purchases displayed");

    }

    private void showHistoryBetweenDates(LocalDate startDate, LocalDate endDate) {
        System.out.println("History between specified date");
        System.out.println("-------------------------");
        List<HistoryItem> historyItems = dao.getHistoryItemList();
        for (HistoryItem item : dao.showDatesBetween(startDate, endDate)) {
                System.out.println(item.getId() + " Date: " + item.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + ", time: " + item.getTime() + ", total: " + item.getTotal());
        }
        System.out.println("-------------------------");
        log.debug("History between dates displayed");
    }

    private void showHistoryPurchases(int id) {
        try {
            List<SoldItem> soldItemList = dao.getSoldItemList((long) id);
            System.out.println("Shopping cart:");
            System.out.println("-------------------------");
            for (SoldItem item : soldItemList) {
                System.out.println(item);
            }
            System.out.println("-------------------------");
            log.debug("Purchase details displayed");
        } catch (Exception e) {
            System.out.println("Index does not exist");
        }
    }



    private void processCommand(String command) throws IOException {
        log.info("Processing new command");
        String[] c = command.split(" ");
        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("c"))
            showCart();
        else if(c[0].equals("t"))
            showTeamTab();
        else if (c[0].equals("p")) {
            cart.submitCurrentPurchase();
        }
        else if (c[0].equals("r"))
            cart.cancelCurrentPurchase();
        else if (c[0].equals("i")) {
            showHistoryPurchases(Integer.parseInt(c[1]));
        }
        else if (c[0].equals("z")) {
            showAllHistory();
        }
        else if (c[0].equals("x"))
            showHistoryOfLast10();
        else if (c[0].equals("a") && c.length == 3) {
            try {
                long idx = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);
                StockItem item = dao.findStockItem(idx);
                if (item != null) {
                    cart.addItem(new SoldItem(item, amount));
                    log.debug("Item added to cart");
                } else {
                    System.out.println("No stock item with id: " + idx);
                }
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        }
        else if (c[0].equals("s")){
            addItemToStock();
        }
        else if (c[0].equals("e")&& c.length == 3){
            try{
                removeItemFromStock(Long.parseLong(c[1]), Integer.parseInt(c[2]));
                log.debug("Item removed from stock");
            }
            catch (Exception e){
                System.out.println("Incorrect input. Error message:" + e.getMessage());
            }
        }
        else if (c[0].equals("b")&& c.length == 3){
            try{
                LocalDate startDate = LocalDate.parse(c[1], DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                LocalDate endDate = LocalDate.parse(c[2], DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                showHistoryBetweenDates(startDate,endDate);
            }
            catch (Exception e){
                System.out.println("Incorrect input. Error message:" + e.getMessage());
            }

        }
        else {
            System.out.println("Unknown command entered");
            log.debug("Faulty command entered");
        }
    }
}
