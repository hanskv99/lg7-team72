# Team lg7-team72:
1. Hans Kristjan Veri
2. Raiki Mänd
3. Alan Kolk

## Homework 1:
Atlassian page.
https://lg7-team72.atlassian.net/jira/software/projects/LT/boards/1
Project page.
https://lg7-team72.atlassian.net/l/c/zqbEdxW0

## Homework 2:
Use Cases: https://lg7-team72.atlassian.net/wiki/spaces/LG7TE/pages/3670017/Homework+2

Jira Task Backlog: https://lg7-team72.atlassian.net/jira/software/projects/LT/boards/1/backlog

## Homework 3:
Here are all our commits and pull-requests with all the required tasks under homework 3.
https://bitbucket.org/hanskv99/lg7-team72/commits/tag/homework-3

## Homework 4:
https://bitbucket.org/hanskv99/lg7-team72/commits/tag/homework-4
the debugger task is under the bitbucket wiki.

## Homework 5:
https://bitbucket.org/hanskv99/lg7-team72/wiki/Homework-5

## Homework 6:
https://bitbucket.org/hanskv99/lg7-team72/wiki/homework-6

## Homework 7:
<Links to the solution>

We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)