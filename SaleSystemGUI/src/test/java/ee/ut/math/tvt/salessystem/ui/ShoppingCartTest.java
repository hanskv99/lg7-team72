package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;


import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThrows;
public class ShoppingCartTest {

    private InMemorySalesSystemDAO dao;
    private ShoppingCart sc;

    @Before
    public final void setUp() {
        dao = new InMemorySalesSystemDAO();
        sc = new ShoppingCart(dao);
    }

    @Test
    public void testAddingExistingItem() {
        StockItem item = new StockItem(33L,"soap","pesuvahend",100,10);
        SoldItem soldItem = new SoldItem(item, 1);
        dao.saveStockItem(item);
        sc.addItem(soldItem);
        int itemQuantityBefore = sc.getAll().get(0).getQuantity();
        SoldItem soldItem1 = new SoldItem(item, 2);
        sc.addItem(soldItem1);
        int itemQuantityAfter = sc.getAll().get(0).getQuantity();
        assertNotEquals(itemQuantityBefore,itemQuantityAfter);
    }

    @Test
    public void testAddingNewItem() {
        StockItem item = new StockItem(33L,"soap","pesuvahend",100,10);
        SoldItem soldItem = new SoldItem(item, 1);
        dao.saveStockItem(item);
        int numberOfItemsBefore = sc.getAll().size();
        sc.addItem(soldItem);
        assertNotEquals(sc.getAll().size(),numberOfItemsBefore);
    }
    @Test
    public void testAddingItemWithNegativeQuantity() {
        assertThrows(Exception.class, () -> {
            StockItem item = new StockItem(34L,"soap","pesuvahend",100,10);
            dao.saveStockItem(item);
            SoldItem soldItem = new SoldItem(dao.findStockItem(34L), -100);
            sc.addItem(soldItem);
        });
    }

    @Test
    public void testAddingItemWithQuantityTooLarge() {
        assertThrows(Exception.class, () -> {
            StockItem item = new StockItem(34L,"soap","pesuvahend",100,10);
            dao.saveStockItem(item);
            SoldItem soldItem = new SoldItem(dao.findStockItem(34L), 100);
            sc.addItem(soldItem);
        });
    }

    @Test
    public void testAddingItemWithQuantitySumTooLarge() {
        assertThrows(Exception.class, () -> {
            StockItem item = new StockItem(34L,"soap","pesuvahend",100,10);
            dao.saveStockItem(item);
            SoldItem soldItem = new SoldItem(dao.findStockItem(34L), 9);
            sc.addItem(soldItem);
            SoldItem soldItem1 = new SoldItem(dao.findStockItem(34L), 2);
            sc.addItem(soldItem1);
        });
    }

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 3);
        int quantityBefore = dao.findStockItem(1L).getQuantity();
        sc.addItem(soldItem);
        sc.submitCurrentPurchase();
        assertNotEquals(dao.findStockItem(1L).getQuantity(), quantityBefore);
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 3);
        sc.addItem(soldItem);
        sc.submitCurrentPurchase();
        Assert.assertEquals(dao.getTransactionCommit(), dao.getTransactionBegin());
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 3);
        int numberOfHistoryItemsBefore = dao.getHistoryItemList().size();
        sc.addItem(soldItem);
        sc.submitCurrentPurchase();
        Assert.assertNotEquals(dao.getHistoryItemList().size(),numberOfHistoryItemsBefore);

    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 3);
        sc.addItem(soldItem);
        sc.submitCurrentPurchase();
        LocalTime historyItemTime = dao.getHistoryItemList().get(0).getTime();
        LocalTime now = LocalTime.now();
        LocalTime nowMinusMaxDelay = now.minusSeconds(1L);
        boolean delayShorterThan1Second = historyItemTime.isBefore(now)&&historyItemTime.isAfter(nowMinusMaxDelay);
        Assert.assertTrue(delayShorterThan1Second);
    }
    @Test
    public void testCancellingOrder() {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 3);
        sc.addItem(soldItem);
        sc.cancelCurrentPurchase();
        SoldItem soldItem1 = new SoldItem(dao.findStockItem(2L), 3);
        sc.addItem(soldItem1);
        sc.submitCurrentPurchase();
        Assert.assertTrue(dao.getSoldItemList(1L).size()==0 && dao.getSoldItemList(2L).size()!=0);
    }
    @Test
    public void testCancellingOrderQuanititesUnchanged() {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 3);
        int itemQuantityBeforeCancelling = dao.findStockItem(1L).getQuantity();
        sc.addItem(soldItem);
        sc.cancelCurrentPurchase();
        int itemQuantityAfterCancelling = dao.findStockItem(1L).getQuantity();
        Assert.assertEquals(itemQuantityBeforeCancelling,itemQuantityAfterCancelling );

    }

    @After
    public final void tearDown() {
    }
}
