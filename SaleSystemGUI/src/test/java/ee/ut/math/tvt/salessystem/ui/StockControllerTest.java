package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.*;

import static org.junit.Assert.*;

public class StockControllerTest {

    private InMemorySalesSystemDAO dao;

    @Before
    public final void setUp() {
        dao = new InMemorySalesSystemDAO();
    }

    @Test
    public void testAddingItemWithNegativeQuantity() {
        assertThrows(Exception.class, () -> {
            InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();
            StockItem item = new StockItem(34L,"soap","pesuvahend",100,-10);
            dao.saveStockItem(item);
        });
    }

    @Test
    public void testAddingExistingItem() {
        InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();
        StockItem item = dao.findStockItems().get(0);
        int itemQuantity = item.getQuantity();
        dao.updateStockItemQuantity(item.getId(),1);
        assertNotEquals(itemQuantity,dao.findStockItems().get(0).getQuantity());
    }

    @Test
    public void testAddingItem() {
        StockItem item = new StockItem(34L,"soap","pesuvahend",3.50,1);
        InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();
        int before = dao.findStockItems().size();
        dao.saveStockItem(item);
        assertNotEquals(before,dao.findStockItems().size());
    }

    @Test
    public void testAddingItemBeginsAndCommitsTransaction() { //Theoretically uncheckable, because no transactions
        dao.saveStockItem(new StockItem(100L,"name","desc",111,11));
        assertEquals(dao.getTransactionBegin(), dao.getTransactionCommit());
    }

    @After
    public final void tearDown() {

    }
}
