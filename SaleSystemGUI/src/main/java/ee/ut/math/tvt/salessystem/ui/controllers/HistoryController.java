package ee.ut.math.tvt.salessystem.ui.controllers;


import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.net.URL;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */

public class HistoryController implements Initializable {
    private static final Logger log = LogManager.getLogger(HistoryController.class);
    private final SalesSystemDAO dao;

    @FXML
    private TableView<HistoryItem> historyTableView;
    @FXML
    private TableView<SoldItem> shoppingCartTableView;
    @FXML
    private Button showAll;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;
    @FXML
    private Button showLast10;
    @FXML
    private Button showBetweenDates;


    public HistoryController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        historyTableView.getSelectionModel().selectedItemProperty().addListener((ChangeListener) (observableValue, oldValue, newValue) -> {
            //Check whether item is selected and set value of selected item to Label
            if(historyTableView.getSelectionModel().getSelectedItem() != null) {
                HistoryItem hi = historyTableView.getSelectionModel().getSelectedItem();
                showShoppingCart(hi);
            }
        });
        log.debug("History tab initialized");
    }
    private void refreshHistory() {
        historyTableView.setItems(FXCollections.observableList(dao.getHistoryItemList()));
        historyTableView.refresh();
    }
    @FXML
    public void showAllEventHandler(){
        refreshHistory();
    }

    private void showShoppingCart(HistoryItem hi) {
        shoppingCartTableView.setItems(FXCollections.observableList(dao.getSoldItemList(hi.getId())));
        shoppingCartTableView.refresh();
    }

    @FXML
    public void showLast10EventHandler() {
        historyTableView.setItems(FXCollections.observableList(dao.showLast10History()));
        historyTableView.refresh();
    }
    @FXML
    public void showBetweenDatesEventHandler() {
        try{
            if (startDate.getValue()==null|| endDate.getValue()==null){

                throw new SalesSystemException("Enter start and end date");
            }
            historyTableView.setItems(FXCollections.observableList(dao.showDatesBetween(startDate.getValue(), endDate.getValue())));
            historyTableView.refresh();
        }
        catch(Exception e){
            errorPopUp(e.getMessage());
        }

    }
    private void errorPopUp(String message) {
        Alert a = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        a.show();
    }


}

