package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private static final Logger log = LogManager.getLogger(StockController.class);
    private final SalesSystemDAO dao;

    @FXML
    private Button addItemButton;
    @FXML
    private Button removeItemButton;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField descriptionField;
    @FXML
    private TextField priceField;
    @FXML
    private TableView<StockItem> warehouseTableView;

    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
        this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
        log.debug("Warehouse tab initialized");
    }

    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            log.debug("Number format error");
            return null;
        }
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            nameField.setText(stockItem.getName());
            priceField.setText(String.valueOf(stockItem.getPrice()));
        }
    }

    @FXML
    public void addItemEventHandler() {
        try {
            StockItem stockItem = getStockItemByBarcode();
            if (stockItem != null) {
                int quantity;
                double price;
                try {
                    quantity = Integer.parseInt(quantityField.getText());
                    price = Double.parseDouble(priceField.getText());
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Illegal input for numeric value");
                }
                if (quantity < 0) throw new IllegalArgumentException("Can not add item with negative quantity");
                dao.beginTransaction();
                if (price != stockItem.getPrice()) dao.updateStockItemPrice(stockItem.getId(), price);
                dao.updateStockItemQuantity(stockItem.getId(), quantity+stockItem.getQuantity());
                dao.commitTransaction();
                resetProductField();
                refreshStockItems();
                log.info("Stock successfully updated");
            } else {
                log.debug("Stock item does not exist in the stock list");
                addNewStockItem();
            }
        } catch (IllegalArgumentException e) {
            errorPopUp(e.getMessage());
            log.debug(e.getMessage());
        }
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
        log.debug("Stock item list was refreshed");
    }

    private void addNewStockItem() {
        try {
            int quantity;
            double price;
            long id;
            try {
                quantity = Integer.parseInt(quantityField.getText());
                price = Double.parseDouble(priceField.getText());
                id = Long.parseLong(barCodeField.getText());
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Illegal input for numeric value");
            }
            StockItem stockItem = new StockItem(
                    id,
                    nameField.getText(),
                    descriptionField.getText(),
                    price,
                    quantity);

            dao.saveStockItem(stockItem);
            log.info("New stock item successfully added");
            resetProductField();
            refreshStockItems();
        } catch (IllegalArgumentException e) {
            errorPopUp(e.getMessage());
            log.debug(e.getMessage());
        }
    }

    private void resetProductField() {
        quantityField.setText("");
        nameField.setText("");
        descriptionField.setText("");
        priceField.setText("");
        log.debug("Product fields reset");
    }

    private void refreshStockItems() {
        warehouseTableView.setItems(FXCollections.observableList(dao.findStockItems()));
        warehouseTableView.refresh();
        log.info("Warehouse list pulled");
    }

    @FXML
    private void removeItem() {
        try {
            StockItem stockItem = getStockItemByBarcode();
            if (stockItem != null) {
                int oldQuantity = stockItem.getQuantity();
                int quantity;
                try {
                    quantity = Integer.parseInt(quantityField.getText());
                } catch (NumberFormatException e) {
                    errorPopUp("Amount must be a number");
                    throw new IllegalArgumentException("Amount must be a number");
                }
                if (quantity <= oldQuantity && quantity >= 0) {
                    dao.beginTransaction();
                    dao.changeQuantity(stockItem.getId(), quantity);
                    dao.commitTransaction();
                    log.debug("Quantity reduced");
                } else {
                    errorPopUp("Amount has to be between 0 and available stock: " + oldQuantity);
                    throw new IllegalArgumentException("Amount has to be between 0 and available stock");
                }
                log.info("Item successfully removed");
                resetProductField();
                refreshStockItems();
            }
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage());
        }
    }

    private void errorPopUp(String message) {
        Alert a = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        a.show();
    }

}
