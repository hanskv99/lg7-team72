package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.StringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private ComboBox<StockItem> nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;

    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        barCodeField.setDisable(true);
        priceField.setDisable(true);
        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
        disableProductField(true);

        this.nameField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsByComboBoxStockItem();
                }
            }
        });
        log.debug("Purchase tab initialized");
    }

    /** Event handler for the <code>new purchase</code> event. */
    @FXML
    protected void newPurchaseButtonClicked() {
        log.info("New sale process started");
        try {
            enableInputs();
            fillNameFieldList();

        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        try {
            shoppingCart.cancelCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        log.info("Sale complete");
        try {
            log.debug("Contents of the current basket:\n" + shoppingCart.getAll());
            shoppingCart.submitCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
            resetProductField();
        } catch (SalesSystemException e) {
            errorPopUp("Purchase failed with exception: "  + e.getMessage());
            log.error(e.getMessage(), e);
        }
    }

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
        log.debug("Inputs enabled");
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
        log.debug("Inputs disabled");
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            log.debug("Barcode number was inserted incorrectly");
            return null;
        }
    }

    private void fillNameFieldList() {
        nameField.setItems(FXCollections.observableList(dao.findStockItems()));
        nameField.setConverter(new StringConverter<StockItem>() {
            @Override
            public String toString(StockItem object) {
                if (object == null) return null;
                return object.getName();
            }

            @Override
            public StockItem fromString(String string) {
                return null;
            }
        });
    }

    private void fillInputsByComboBoxStockItem() { //Item from selected ComboBox
        StockItem stockItem = nameField.getValue();
        if (stockItem != null) {
            barCodeField.setText(String.valueOf(stockItem.getId()));
            priceField.setText(String.valueOf(stockItem.getPrice()));
        }
        else {
            log.debug("Item from nameField was null");
        }
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler(){
        // add chosen item to the shopping cart.
        try {
            StockItem stockItem = getStockItemByBarcode();
            if (stockItem != null) {
                int quantity = 0;
                try {
                    quantity = Integer.parseInt(quantityField.getText());
                } catch (NumberFormatException e) {
                    errorPopUp("Amount has to be a number");
                    throw new IllegalArgumentException("Amount has to be a number");
                }
                if (quantity > stockItem.getQuantity()) {
                    errorPopUp("Quantity exceeds the available stock of: " + stockItem.getQuantity());
                    throw new IllegalArgumentException("Quantity exceeds the available stock of: " + stockItem.getQuantity());
                }
                int stockItemQuantity = dao.findStockItem(stockItem.getId()).getQuantity();
                SoldItem existingItem = null;
                boolean exists = false;
                for (SoldItem soldItem : shoppingCart.getAll()) {
                    if (soldItem.getBarcode() == stockItem.getId()) {
                        exists = true;
                        if ((soldItem.getQuantity() + quantity) > stockItemQuantity) {
                            errorPopUp("Quantity exceeds the available stock of: " + stockItem.getQuantity());
                            throw new IllegalArgumentException("Quantity exceeds the available stock of: " + stockItem.getQuantity());
                        }
                        existingItem = soldItem;
                    }
                }
                if(exists && (existingItem.getQuantity() + quantity) < 0) {
                    errorPopUp("Can't remove more than is in the cart");
                    throw new IllegalArgumentException("Can't remove more than is in the cart");
                }

                if (!exists) {
                    if (quantity < 0) {
                        errorPopUp("Quantity can only be negative for removing if item is in cart");
                        throw new IllegalArgumentException("Quantity can only be negative if item is in cart");
                    }
                }
                shoppingCart.addItem(new SoldItem(stockItem, quantity));
                purchaseTableView.refresh();
                if (quantity < 0) log.info("Item removed from shopping cart");
                else log.info("Item added to shopping cart");
            }
        } catch (IllegalArgumentException e) {
            log.debug(e.getMessage());
        }
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.nameField.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        nameField.getSelectionModel().clearSelection();
        priceField.setText("");
        log.debug("Fields reset");
    }

    private void errorPopUp(String message) {
        Alert a = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        a.show();
    }

}
