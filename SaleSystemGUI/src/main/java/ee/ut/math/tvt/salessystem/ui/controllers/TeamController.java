package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable {

    private static final Logger log = LogManager.getLogger(TeamController.class);

    @FXML
    private Label teamName;
    @FXML
    private Label leaderName;
    @FXML
    private Label leaderEmail;
    @FXML
    private Label membersText;
    @FXML
    private ImageView image;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties")) {
            Properties properties = new Properties();
            properties.load(inputStream);
            teamName.setText(properties.getProperty("teamnametext"));
            leaderName.setText(properties.getProperty("leadertext"));
            leaderEmail.setText(properties.getProperty("leaderemailtext"));
            membersText.setText(properties.getProperty("memberstext"));
            image.setImage(new Image(properties.getProperty("imageurl")));
            log.debug("Team tab initialized");
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
